const EMAIL_URL = "https://us-central1-allanbildev.cloudfunctions.net/sendMail"

let questions = null

function redirect() { window.location.href = "404.html" }

function getParam(name) {
    const queryString = window.location.search
    const urlParams = new URLSearchParams(queryString)
    return urlParams.get(name)
}

function loadPDF() {
    let id = getParam('id')
    let frame = document.getElementById("frame")
    fetch('polys/' + id + '.pdf')
    .then(response => {
        if (response.status == 404) { redirect() }
        else { frame.setAttribute("src", "polys/" + id + ".pdf") }
    })
}

function loadQuestions()  {
    if (questions == null) {
        let id = getParam('id')
        fetch('files/' + id + '.json')
        .then(response => response.json())
        .then(json => {
            questions = json
            displayRandomQuestion()
        })
        .catch(err => redirect())
    }
    else if (questions.length == 0) { displayNoMoreQuestions() }
    else { displayRandomQuestion() }
}

function displayRandomQuestion() { 
    let i = Math.floor(Math.random() * questions.length)
    let q = questions.splice(i, 1)
    displayQuestion(q[0]) 
}

function displayQuestion(question) {
    document.getElementById('validateQuestionButton').style.display = "inline"
    document.getElementById('questionTitle').innerHTML = question.title
    let answers = ""
    let shuffled = shuffle(question.answers)
    for (let i = 0; i < shuffled.length; i++) {
        const a = shuffled[i]
        answers += '<div class="form-check">\n'
        answers += '<input class="form-check-input" type="radio" name="answer" id="answer' + i + '" value="' + i + '">\n'
        answers += '<label class="form-check-label" for="answer' + i + '"><p><span class=\"math inline\">' + a.text + '</span><img class="truthIndicator" style="visibility: hidden;" src="images/' + (a.isValid ? "valid" : "invalid") +'.png" /></p></label>\n'
        answers += '</div>\n'
    }
    document.getElementById('questionProposition').innerHTML = answers
    MathJax.startup.document.state(0);
    MathJax.texReset();
    MathJax.typeset();
}

function displayNoMoreQuestions() {
    document.getElementById('questionTitle').innerHTML = ""
    document.getElementById('questionProposition').innerHTML = '<p>Il n\'y a plus de questions...<br /><a href="/">Revenir à l\'accueil</a></p>'
}

function validate(e) {
    e.preventDefault()
    for (const el of document.getElementsByClassName('truthIndicator')) {
        el.style.visibility = "visible"
    }

    document.getElementById('validateQuestionButton').style.display = "none"
    document.getElementById('nextQuestionButton').style.display = "inline"
}

function nextQuestion(e) {
    e.preventDefault()
    document.getElementById('nextQuestionButton').style.display = "none"
    loadQuestions()

}

function shuffle(a) {
    var c = a.length, temp, index
    while (c > 0) {
        index = Math.floor(Math.random() * c)
        c--
        temp = a[c]
        a[c] = a[index]
        a[index] = temp
    }
    return a
}

function tryToSendMail(e) {
    document.getElementById('successAlert').style.display = 'none'
    document.getElementById('errorAlert').style.display = 'none'
    console.log("ther")
    e.preventDefault()
    let email = document.getElementById('emailInput').value
    let message = document.getElementById('messageInput').value
    if (validateEmail(email) && message != null && message != undefined && message.length > 0) {
        sendMail(email, message)
        document.getElementById('successAlert').style.display = 'block'
    }
    else {
        document.getElementById('errorAlert').style.display = 'block'
    }
}

function sendMail(from, message) {
    fetch(EMAIL_URL, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: 'post',
        body: JSON.stringify({ from: from, text: message })
    })
    .then(response => { })
}

function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}