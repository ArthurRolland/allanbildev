const functions = require('firebase-functions');
const admin = require('firebase-admin');
const nodemailer = require('nodemailer');
const cors = require('cors')({origin: true});
admin.initializeApp();

/**
* Here we're using Gmail to send 
*/
let transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'prof.bilde@gmail.com',
        pass: 'ToTo1234!'
    }
});

exports.sendMail = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        // getting dest email by query string
        const payload = req.body;
        const dest = "arthurrolland@me.com"
        
        const mailOptions = {
            from: payload.from, 
            to: dest,
            subject: 'AllanBildev.com: New Message from ' + payload.from, 
            text: payload.text
        };

        // returning result
        return transporter.sendMail(mailOptions, (erro, info) => {
            if(erro){
                return res.send(erro.toString());
            }
            return res.send('Sended');
        });
    });    
});
